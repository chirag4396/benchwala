<?php
Route::get('/', function () {
    return view('user.index');
});

Route::get('/products', function () {
    return view('user.products');
});

Route::get('/single', function () {
    return view('user.single');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/login', function() {
	return view('admin.login');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function() {
	Route::get('/', function(){
		return view('admin.dashboard');
	})->name('home');

	Route::get('images', function (){	
		return view('admin.upload');
	})->name('g-images');

	Route::resource('category', 'CategoryController');
	Route::post('images', 'UserDetailController@images')->name('images');
	Route::resource('enquiry', 'EnquiryController');
});