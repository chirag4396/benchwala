<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $primaryKey = 'cat_id';

    protected $fillable = ['cat_name', 'cat_img'];

    CONST CREATED_AT = 'cat_created_at';

    CONST UPDATED_AT = 'cat_updated_at';
}
