<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Traits\GetData;
use Illuminate\Database\QueryException;

class CategoryController extends Controller
{
    use GetData;

    protected $res = ['msg' => 'error'];

    protected $path = 'images/categories/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.view_categories')->with(['categories' => Category::get()]);                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_category');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        try {
            $cat = $this->changeKeys('cat_',$r->all());
            $cat['cat_name'] = ucfirst($cat['cat_name']);                            

            if ($r->hasFile('img')) {
                list($regular) = $this->uploadFiles($r, $cat['cat_name'], 'img', [$this->path]);
                $cat['cat_img'] = $regular;
            }        

            $this->res['msg'] = Category::create($cat) ? 'success' : 'error';

        } catch (QueryException $e) {
            $this->res['msg'] = 'error';        
        }
        return $this->res;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
