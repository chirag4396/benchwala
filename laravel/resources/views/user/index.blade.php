@extends('user.layouts.master')

@push('header')
<script src="js/jssor.slider-27.1.0.min.js" type="text/javascript"></script>
<script type="text/javascript">
	jssor_1_slider_init = function() {

		var jssor_1_SlideshowTransitions = [
		{$Duration:1200,$Zoom:1,$Easing:{$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad},$Opacity:2},
		{$Duration:1000,$Zoom:11,$SlideOut:true,$Easing:{$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
		{$Duration:1200,$Zoom:1,$Rotate:1,$During:{$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
		{$Duration:1000,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Zoom:$Jease$.$InQuint,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuint},$Opacity:2,$Round:{$Rotate:0.8}},
		{$Duration:1200,x:0.5,$Cols:2,$Zoom:1,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
		{$Duration:1200,x:4,$Cols:2,$Zoom:11,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
		{$Duration:1200,x:0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Opacity:2,$Round:{$Rotate:0.5}},
		{$Duration:1000,x:-4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InQuint,$Zoom:$Jease$.$InQuart,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuint},$Opacity:2,$Round:{$Rotate:0.8}},
		{$Duration:1200,x:-0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Opacity:2,$Round:{$Rotate:0.5}},
		{$Duration:1000,x:4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InQuint,$Zoom:$Jease$.$InQuart,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InQuint},$Opacity:2,$Round:{$Rotate:0.8}},
		{$Duration:1200,x:0.5,y:0.3,$Cols:2,$Zoom:1,$Rotate:1,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.7}},
		{$Duration:1000,x:0.5,y:0.3,$Cols:2,$Zoom:1,$Rotate:1,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Top:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.7}},
		{$Duration:1200,x:-4,y:2,$Rows:2,$Zoom:11,$Rotate:1,$Assembly:2049,$ChessMode:{$Row:28},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.7}},
		{$Duration:1200,x:1,y:2,$Cols:2,$Zoom:11,$Rotate:1,$Assembly:2049,$ChessMode:{$Column:19},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.8}}
		];

		var jssor_1_options = {
			$AutoPlay: 1,
			$SlideshowOptions: {
				$Class: $JssorSlideshowRunner$,
				$Transitions: jssor_1_SlideshowTransitions,
				$TransitionsOrder: 1
			},
			$ArrowNavigatorOptions: {
				$Class: $JssorArrowNavigator$
			},
			$ThumbnailNavigatorOptions: {
				$Class: $JssorThumbnailNavigator$,
				$Rows: 2,
				$SpacingX: 14,
				$SpacingY: 12,
				$Orientation: 2,
				$Align: 156
			}
		};

		var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

		/*#region responsive code begin*/

		var MAX_WIDTH = 960;

		function ScaleSlider() {
			var containerElement = jssor_1_slider.$Elmt.parentNode;
			var containerWidth = containerElement.clientWidth;

			if (containerWidth) {

				var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

				jssor_1_slider.$ScaleWidth(expectedWidth);
			}
			else {
				window.setTimeout(ScaleSlider, 30);
			}
		}

		ScaleSlider();

		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		/*#endregion responsive code end*/
	};
</script>
@endpush

@section('content')
<div class="main-content">
	<div class="col-lg-3  col-md-12 m-t menu-bg" style="padding: 0;">
		<div class="col-md-12">
			<div class="widget category-list padding" style="border-right: none;">
				<h4 class="widget-header">All Category</h4>
				<ul class="category-list">
					<li><a href="#">Chairs</a></li>
					<li><a href="#">Chairs</a></li>
					<li><a href="#">Chairs</a></li>
				</ul>
			</div>
		</div>
	</div>
	<aside style="padding:0" class="col-md-12 col-lg-9 " id="fh5co-hero" class="js-fullheight">
		<div class="flexslider js-fullheight">
			<ul class="slides">
				<li style="background-image: url('images/2.jpg'); width: 100%; display: block; z-index: 2; height: 500px;" data-thumb-alt="">
					<div class="container">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">

						</div>
					</div>
				</li>
				<li style="background-image: url('images/3.png');width: 100%; display: block; z-index: 2; height: 500px;" data-thumb-alt="">
					<div class="container">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">

						</div>
					</div>
				</li>
				<li style="background-image: url('images/4.jpg');width: 100%; display: block; z-index: 2; height: 500px;" data-thumb-alt="">
					<div class="container">
						<div class="col-md-6 col-md-offset-3 col-md-pull-3 js-fullheight slider-text">

						</div>
					</div>
				</li>
			</ul>
			<ol class="flex-control-nav flex-control-paging"><li><a href="#" class="flex-active">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li></ol>
			<ul class="flex-direction-nav"><li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li><li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li></ul>
		</div>
	</aside>

	<div id="fh5co-services" class="fh5co-bg-section">

		<div class="container">
			<h2 class="head1">Explore Our Furniture </h2>

			<div class="border-line3"></div>

			<div class="row">
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/11.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/12.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/13.jpg"></a>




					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/14.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/15.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/16.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/17.jpg"></a>

					</div>
				</div>
				<div class="col-md-3 col-sm-4 text-center">
					<div class="feature-center animate-box" data-animate-effect="fadeIn">
						<a href="category.php"><img src="images/18.jpg"></a>

					</div>
				</div>

			</div>
		</div>
	</div>
	<div id="fh5co-testimonial" class="fh5co-bg-section ">
		<div class="container">
			<div class="row animate-box fadeInUp animated-fast">
				<div class="col-md-8 col-md-offset-2 text-center">

				</div> 
			</div>
		</div>

		<div class="carousel-reviews broun-block">
			<div class="container">
				<div class="row">
					<div id="carousel-reviews" class="carousel slide" data-ride="carousel">

						<div class="carousel-inner">							
							<div class="item active">
								<div class="col-md-4 col-sm-6">
									<div class="block-text rel zmin">
										<div class="agileits_w3layouts_testimonials_grid ">
											<img src="images/c1.png" alt=" " class="img-responsive">
										</div>
										<div class="clear"></div>
										<h3 style="text-align: center;" title="" href="#">Ponny Chacko</h3>
										<h4>(ui-designer)</h4>
										<p>Never before has there been a good film portrayal of ancient Greece's favourite myth. So why would Hollywood start now? This latest attempt at bringing the son of Zeus to the big screen is brought to us by X-Men: The last Stand director Brett Ratner. If the name of the director wasn't enough to dissuade ...</p>
										<ins class="ab zmin sprite sprite-i-triangle block"></ins>
									</div>
								</div>
							</div>          
						</div>

					</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection