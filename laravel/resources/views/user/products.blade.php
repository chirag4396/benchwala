@extends('user.layouts.master')


@push('header')
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('') }}">	 --}}
@endpush

@section('content')

<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(images/q5.jpg);">

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="display-t">
					<div class="display-tc animate-box" data-animate-effect="fadeIn">
						<h1>Products</h1>

					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div id="fh5co-product">
	<div class="row animate-box">
		<div class="col-md-12  text-center fh5co-heading">

			<p>Get a fresh, perfectly coordinated look for your Place with these gorgeous new designs.</p>

		</div>
	</div>

	<div class="container pro">

		<div class="row">
			<div class="col-md-3">
				<div class="widget category-list">
					<h4 class="widget-header">All Category</h4>
					<ul class="category-list">
						<li><a href="product.html">CLASSROOM
							BENCHES


						</a></li>
						<li><a href="product.html">KIDS SCHOOL
							FURNITURE
						</a></li> 
						<li><a href="product.html">HOSTEL FURNITURE 
						</a></li>
						<li><a href="product.html">LIBRARY FURNITURE

						</a></li>
						<li><a href="product.html">
							STORAGE
							CUPBOARDS
						</a></li>

						<li><a href="product.html">OFFICE
							TABLES

						</a></li>


						<li><a href="product.html">LABORATORY  FURNITURE

						</a></li>

						<li><a href="product.html">WRITING PAD
							CHAIRS

						</a></li>
						<li><a href="product.html">  AUDITORIUM CHAIRS
						</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-9 ">
				<h4 class="widget-header col-md-8 col-xs-12">KIDS SCHOOL
				FURNITURE</h4>

				<div class="col-md-4 col-xs-12"><span style="color:red">Sort By</span>
					<select>

						<option value="1">Model No.</option>
						<option value="1">Price High to Low</option>
						<option value="1">Price Low to High</option>


					</select>
				</div>

				<div class="clear"></div>
				<h6><span style="color:red">Purchase Order:</span> We do accept PO from Office; Corporate; Institute & Hospitals.</h6>
				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(images/product-1.jpg);">
							<div class="inner">
								<p>

									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">SF - A 01</a></h3>
							<a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-outline btn-lg">Enquiry</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(images/product-2.jpg);">
							<span class="sale">Sale</span>
							<div class="inner">
								<p>

									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">SF - A 01</a></h3>
							<a href="#" class="btn btn-primary btn-outline btn-lg">Enquiry</a>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(images/product-3.jpg);">
							<div class="inner">
								<p>

									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">SF - A 01</a></h3>
							<a href="#" class="btn btn-primary btn-outline btn-lg">Enquiry</a>
						</div>
					</div>
				</div>

				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(images/product-3.jpg);">
							<div class="inner">
								<p>

									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">SF - A 01</a></h3>
							<a href="#" class="btn btn-primary btn-outline btn-lg">Enquiry</a>
						</div>
					</div>
				</div>


				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(images/product-3.jpg);">
							<div class="inner">
								<p>

									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">SF - A 01</a></h3>
							<a href="#" class="btn btn-primary btn-outline btn-lg">Enquiry</a>
						</div>
					</div>
				</div>


				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(images/product-3.jpg);">
							<div class="inner">
								<p>

									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">SF - A 01</a></h3>
							<a href="#" class="btn btn-primary btn-outline btn-lg">Enquiry</a>
						</div>
					</div>
				</div>			
			</div>
		</div>












			<!-- <div class="row">
				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(images/product-4.jpg);">
							<div class="inner">
								<p>
									 
									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">Alato Cabinet</a></h3>
							<span class="price">$800</span>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(images/product-5.jpg);">
							<div class="inner">
								<p>
									 
									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">Earing Wireless</a></h3>
							<span class="price">$100</span>
						</div>
					</div>
				</div>
				<div class="col-md-4 text-center animate-box">
					<div class="product">
						<div class="product-grid" style="background-image:url(images/product-6.jpg);">
							<div class="inner">
								<p>
									 
									<a href="single.html" class="icon"><i class="icon-eye"></i></a>
								</p>
							</div>
						</div>
						<div class="desc">
							<h3><a href="single.html">Sculptural Coffee Table</a></h3>
							<span class="price">$960</span>
						</div>
					</div>
				</div>
			</div> -->
		</div>
	</div>

	@endsection

	@push('footer')
{{-- <script type="text/javascript" src="{{ asset('') }}"></script> --}}
@endpush