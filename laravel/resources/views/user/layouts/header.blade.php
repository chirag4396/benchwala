<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Benchwala </title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta name="author" content="" />
  <!-- jQuery -->
  <script src="js/jquery.min.js"></script>
  
  <!-- Animate.css -->
  <link rel="stylesheet" href="css/animate.css">
  <!-- Icomoon Icon Fonts-->
  <link rel="stylesheet" href="css/icomoon.css">
  <!-- Bootstrap  -->
  <link rel="stylesheet" href="css/bootstrap.css">

  <!-- Flexslider  -->
  <link rel="stylesheet" href="css/flexslider.css">

  <!-- Owl Carousel  -->
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">

  <!-- Theme style  -->
  <link rel="stylesheet" href="css/style.css">
  <!-- Modernizr JS -->
  <script src="js/modernizr-2.6.2.min.js"></script>
  @stack('header')
</head>
<body>
  <div class="fh5co-loader" style="display: none;"></div>
  <div id="page"><a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle fh5co-nav-white"><i></i></a><div id="fh5co-offcanvas">
    <ul>
      <li><a href="index.php">Home</a></li>
      <li><a href="about.php">About us</a></li>
      <li><a href="product.php">Products</a></li>
      <li><a href="gallery.php">Gallery</a></li>
      <li><a href="feedback.php">Feedback form</a></li>
      <li><a href="Steelfab.pdf" target="_blank">Download Catalogue</a></li>
      <li><a href="contact.php">Contact</a></li>
    </ul>
  </div>
  <nav class="fh5co-nav navbar-fixed-top" role="navigation">

    <div class="top-header">
      <div class="container">
        <div class="col-md-9">
          <p>CALL: <?php 
            // $qry = 'select ad_city,ad_mobile from address_details';
            // $res = $conn->query($qry);
            // if($res->num_rows){
            //   while($row = $res->fetch_assoc())
            //   {
            //     echo $row['ad_city'].':'.$row['ad_mobile'].'|';
            //   }
            // }  
          ?></p>
        </div>
        <div class="col-md-3">
          <p class="email" style="color: yellow;"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Mail Us - 
            <?php 
              // $qry = 'select ad_email from address_details where ad_city="PUNE(HO)"';
              // $res = $conn->query($qry);
              // if($res->num_rows){
              //   while($row = $res->fetch_assoc())
              //   {
              //     echo $row['ad_email'];
              //   }
              // }  
            ?>
          </p>
        </div>
      </div>
    </div>
    <div class=" top-header2">
      <div class="row">
        <div class="col-md-3 col-xs-2">
          <div id="fh5co-logo"><a href="index"><img src="images/logoo.png"></a></div>
        </div>
        <div class="col-md-7 col-xs-6 text-center menu-1 no-padding">
          <ul>
            <li><a href="index">Home</a></li>
            <li><a href="about">About us</a></li>
            <li><a href="product">Products</a></li>
            <li><a href="gallery">Gallery</a></li>
            <li><a href="feedback">Feedback form</a></li>
            <li><a href="Steelfab.pdf" target="_blank">Download Catalogue</a></li>
            <li><a href="contact">Contact</a></li>
          </ul>
        </div>
        <div class="col-md-2 col-xs-4 text-right hidden-xs menu-2">
          <form id="searchForm">
            <ul>
              <li class="search">

                <div class="input-group">

                  <form id = "searchForm">

                    <input type="text" name="sname" id="content" placeholder="Search..">
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit" id="bsearch"><i class="icon-search"></i></button>
                    </span>
                  </form>
                </div>


              </li>

            </ul>
          </form>
        </div>
      </div>

    </div>
  </nav>