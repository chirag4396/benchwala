@extends('admin.layouts.master')
@section('title')
Create Sub Category
@endsection

@php
	$ID = 'sub-category';
@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
<style type="text/css">
img {
	object-fit: contain;
}
</style>
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Create New Sub Category</h3>
		</div>
		<div class="pull-right">
			<a href = "{{ route('admin.'.$ID.'.index') }}" class="btn btn-danger">Back</a>
		</div>
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<br />
					<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/form-data">
						<input type="hidden" name="id" value = "{{ $sub->scat_id }}">
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Select Category
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								<select class="form-control" name = "cat_id">
									<option value = "-1">Select</option>
									@foreach (App\Models\Category::get() as $cat)
									<option {{ $cat->cat_id == $sub->scat_cat_id ? 'selected' : '' }} value = "{{ $cat->cat_id }}">{{ $cat->cat_title }}</option>
									@endforeach
								</select>								
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Sub Category Name
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<input type="text" class="form-control col-md-7 col-xs-12" name = "title" value = "{{ $sub->scat_title }}">
							</div>
						</div>						
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">Sub Thumbnail
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">								
								<img src="{{ asset(empty($sub->scat_img_path) ? 'images/no-image.png' : $sub->scat_img_path) }}" width="300" height="150" id = "sub-picPreview">
								<div class="clearfix"></div>
								<input type = "file" id ="sub-pic" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="sub_picture">
								<label class="btn btn-success" for = "sub-pic">Choose Image</label>
							</div>
						</div>
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Edit
							</button>							
						</div>					
					</form>					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script>
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID.'.index') }}',
		validation:false,	
		type : 2,
		processResponse : function (data) {
			console.log(data);
		}
	});	
	imageUpload('sub-pic');

</script>
@endpush