<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/formdata">
	<div class="form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Category Title
		</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" class="form-control col-md-7 col-xs-12" name = "name">
		</div>
	</div>
	<div class="form-group">
	    <div class="text-center">
	        <img src="{{ asset('images/no-image.png') }}" id = "imgPreview" class="img-upload">
	        <div class="clearfix"></div>
	        <input type = "file" id ="img" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="img">
	        <label class="btn btn-success" for = "img">Choose Image</label>
	    </div>
	</div> 					
	<div class="ln_solid">
	</div>
	<div class="form-group text-center">							
		<button type="submit" class="btn btn-success">Add
		</button>							
	</div>					
</form>	