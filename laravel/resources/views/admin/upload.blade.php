@extends('admin.layouts.master')
@php
$ID = 'images';
@endphp
@push('header')
<script>
	ID = '{{ $ID }}';
</script>
<style type="text/css">
img {
	object-fit: contain;
}
</style>
@section('title')
Create {{ ucwords($ID) }}
@endsection
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
@endpush
@section('content')
<div class="right_col" role="main">	
	<div class="page-title">
		<div class="title_left">
			<h3> Register {{ ucwords($ID) }}</h3>
		</div>		
	</div>
	<div class="clearfix">
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">				
				<div class="x_content">
					<br />
					<form id = "{{ $ID }}Form" class="form-horizontal form-label-left" enctype="multipart/form-data">					
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">User Image
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12 text-center">
								<img src="{{ asset('images/blank-user.jpg') }}" width="100" height="150" id = "user-imgPreview">
								<div class="clearfix"></div>
								<input type = "file" id ="user-img" accept="image/png, image/jpeg, image/jpg" class="hidden form-control1" name="user_img" multiple>
								<label class="btn btn-success" for = "user-img">Choose User Image</label>
							</div>
						</div>
						<div class="ln_solid">
						</div>
						<div class="form-group text-center">							
							<button type="submit" class="btn btn-success">Add
							</button>							
						</div>					
					</form>					
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('footer')
<script src="{{ asset('js/select2.min.js') }}"></script>
<script>
	$('#{{ $ID }}Form').CRUD({
		url : '{{ route('admin.'.$ID) }}',
		validation:false,	
		processResponse : function (data) {
			console.log(data);
		}
	});	
	imageUpload('user-img');

</script>
@endpush