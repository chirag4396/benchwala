@extends('admin.layouts.master')

@section('title')
Employees
@endsection
@push('header')
<!-- Datatables -->
<link href="{{ asset('admin-assets/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin-assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@php
$ID = 'employee';
@endphp
<script>
  var ID = '{{ $ID }}';
</script>
@endpush

@section('content')

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Employees</h3>
      </div>      
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_content">            
            <table id="{{ $ID }}Table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Sr. No.</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Mobile</th>
                  <th>Categories</th>
                  <th>Joining Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($users as $k => $e)
                <tr id = "tr-{{ $e->enq_id }}">
                  <td>{{ ++$k }}</td>
                  <td>{{ $e->name }}</td>
                  <td>{{ $e->email }}</td>
                  <td>{{  $e->detail->ud_mobile or '-' }}</td>
                  @if (isset($e->detail->ud_category))
                  @php
                  $cats = App\Models\SubCategory::whereIn('scat_id', explode(',',$e->detail->ud_category))->get();
                  $t = [];
                  $mc = [];
                  foreach ($cats as $k => $v) {   
                    $t[] = $v->scat_title;
                  }
                  $title = implode(', ',$t);
                  @endphp
                  <td>{{ $title }}</td>
                  @else
                  <td>-</td>                    
                  @endif
                  <td>{{ Carbon\Carbon::parse($e->created_at)->toDayDateTimeString() }}</td>
                  <td>                    
                    <a href="{{ route('admin.employee.edit', ['id' => $e->id]) }}" class="btn btn-info btn-xs" ><i class="fa fa-pencil"></i> View</a> | 
                    <form action="{{ route('admin.employee.destroy', ['id' => $e->id]) }}" method="post">
                      <input type="hidden" name="_method" value="delete">
                      {{ csrf_field() }}
                      <button class="btn btn-danger btn-xs" ><i class="fa fa-trash"></i> Delete</button>
                    </form>
                  </td>
                </tr>
                @empty
                <tr>
                  <td colspan = "7" class="text-center">No {{ $ID }} Found</td>
                </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade" id="{{ $ID }}Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="{{ $ID }}ModalLabel">View {{ ucwords($ID) }}</h4>
          </div>
          <div class="modal-body">
            <form id = "{{ $ID }}Form" class="form-horizontal form-label-left">              
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Full Name:</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input readonly type="text"  class="form-control no-borders"  name="full_name">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Category:</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input readonly type="text"  class="form-control no-borders"  name="scat_title">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name:</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input readonly class="form-control no-borders" type="text" name="title">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile:</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input readonly class="form-control no-borders" type="number" name="mobile" id ="enq-mobile">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email:</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input readonly class="form-control no-borders" type="email" name="email" required>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">City:</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input readonly class="form-control no-borders" type="text" name="city" id ="enq-mobile">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">From:</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input readonly class="form-control no-borders" type="date" name="from">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">To:</label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <input readonly class="form-control no-borders" type="date" name="to">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Query:
                </label>
                <div class="col-md-8 col-sm-6 col-xs-12">
                  <textarea readonly class="form-control no-borders" rows="3" name = "query">Query</textarea>
                </div>
              </div>
            </form>             
            <div class="clearfix"></div>          
          </div>          
        </div>
      </div>
    </div>

    <div class="modal fade" id="{{ $ID }}DModal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header text-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Are You Sure?</h4>
            <br>
            <div>
              <form id = "{{ $ID }}DForm">                
                <button type = "button" class="btn btn-danger" id = "yes">Yes</button>
                <button type = "button" class="btn btn-success" data-dismiss="modal">No</button>
              </form>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@push('footer')
<script src="{{ asset('admin-assets/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
{{-- <script src="{{ asset('admin-assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script> --}}
<script src="{{ asset('admin-assets/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('admin-assets/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('admin-assets/jszip/dist/jszip.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/pdfmake.min.js') }}"></script>
<script src="{{ asset('admin-assets/pdfmake/build/vfs_fonts.js') }}"></script>

<script type="text/javascript">  
  var update = "{{ url('admin/'.$ID) }}",
  deleteU = "{{ url('admin/'.$ID) }}",
  store = "{{ route('admin.'.$ID.'.store') }}";

  Table.init(ID);
  function geturl(id) {
    var link = '{{ route('admin.enquiry.index') }}/'+id+'/edit';
    return link;
  }
  function view(id) {
    $.get(geturl(id), function(data) {
      $('#'+ID+'Modal').modal('toggle');
      $.each($('#'+ID+'Form')[0].elements,function(k, v) {
        var name = $(v).attr('name');        
        $(v).val(data[name]);
      });
    });
  }  

</script>
@endpush