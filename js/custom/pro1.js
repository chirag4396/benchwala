function response(data,msg){
	if(data.indexOf('OK') >= 0){
		$('#enquiryMessage').show().html(msg);          
        // fetchData();
        $('#enquiryForm')[0].reset();

        window.setTimeout(function(){
        	$('#myModal').modal('toggle');
        	$('#enquiryMessage').hide().html('');
        },2000);
    }else{
    	$('#enquiryMessage').show().html('Something Went Wrong');
    	window.setTimeout(function(){
    		$('#enquiryMessage').hide().html('');
    	},3000);
    }
}
function productEnquiry(id){
	$('#myModal').modal('toggle');
	$('#id').val(id);
	console.log(id);
	$('#enquiryForm').on({
		'submit' : function(e){
			e.preventDefault();
			var formdata= new FormData(this);
			$.ajax({
				url : 'req/add_enquiry.php',
				type : 'post',
				data : formdata,
				contentType:false,
				processData:false,
				success: function(data){    
					// console.log(data); 
					response(data,'Successfully Inserted');
				} 
			});
		}
	});
	// $('#myModal').modal('toggle');
}


function fetchProduct(catId = null)
{

	var searchText = $('#content').val();
	var sort = $('#sort').val();
	$('#product_box').html('');

	// console.log(catId);
	$.ajax({
		url:'req/product_fetch.php',
		type:'post',
		data:{'searchText' : searchText, 'catId' : catId, 'sort': sort},
		dataType:'json',
		success:function(data){
			// console.log(data);
			product = [];
			i = 1;
			$.each(data,function(key,val){
				div ='<div class="col-md-4 text-center animate-box fadeInUp animated-fast">'
				+'<div class="product">'
				+'<div class="product-grid" style="background-image:url(uploads/'+val.pro_image+');">'
				+'<div class="inner">'
				+'<p>'
				+'<a href="single.php?id='+val.pro_id+'" class="icon"><i class="icon-eye"></i></a>'
				+'</p>'
				+'</div>'
				+'</div>'
				+'<div class="desc">'
				+'<h3><a href="single.php?id='+val.pro_id+'">'+val.pro_model_no+'</a></h3>'
				+'<button onclick="productEnquiry('+val.pro_id+')" class="btn btn-primary btn-outline btn-lg">Enquiry</button>'
				+'</div>'
				+'</div>'
				+'</div>';

				product.push(div);
			});
			$('#product_box').html(product);

		}
	});
}
fetchProduct();
$('#sort').on({
	change : function(){
		fetchProduct();
	}
});

$('#categories li').each(function(v,l){
	$(l).on({
		'click' : function(){
			var value = $(this).attr('data-id');
			// console.log(value);
			fetchProduct(value);

		}
	});
});
// function sortProduct()
// {
// 	$.ajax({
// 		url:'req/product_fetch.php',
// 		type:'get',
// 		data:{'hint':2},
//         dataType:'json',
//         success:function(data){
//         	console.log(data);
//         	product = [];
//         	i = 1;
//         	$.each(data,function(key,val){
//         		div ='<div class="col-md-4 text-center animate-box fadeInUp animated-fast">'
//         		+'<div class="product">'
//         		+'<div class="product-grid" style="background-image:url(uploads/'+val.pro_image+');">'
//         		+'<div class="inner">'
//         		+'<p>'
//         		+'<a href="single.php?id='+val.pro_id+'" class="icon"><i class="icon-eye"></i></a>'
//         		+'</p>'
//         		+'</div>'
//         		+'</div>'
//         		+'<div class="desc">'
//         		+'<h3><a href="single.php?id='+val.pro_id+'">'+val.pro_model_no+'</a></h3>'
//         		+'<button onclick="productEnquiry('+val.pro_id+')" class="btn btn-primary btn-outline btn-lg">Enquiry</button>'
//         		+'</div>'
//         		+'</div>'
//         		+'</div>';
//         		// if((i++ % 3) == 0)
//           //       {
//           //           div +='<div class="clearfix"> </div>';
//           //       }
//         		product.push(div);
//         	});
//             $('#product_box').html(product);

//         }
//     });
// }
// function category(){
// 	$.ajax({
// 		url: 'req/product_fetch.php',
// 		type: 'get',
// 		data:{'hint':2},
// 		// dataType:'json',
// 		success:function(data){
// 			console.log(data);
// 		}
// 	});
// }