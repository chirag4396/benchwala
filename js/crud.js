$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});
var handleDataTableButtons = function(id) {
	var i = '#'+id+'Table';
	if ($(i).length) {
		$(i).DataTable().destroy();
		$(i).DataTable({
			dom: "Bfrtip",
			buttons: [
			{
				extend: "copy",
				className: "btn-sm"
			},
			{
				extend: "csv",
				className: "btn-sm"
			},
			{
				extend: "excel",
				className: "btn-sm"
			},
			{
				extend: "pdfHtml5",
				className: "btn-sm"
			},
			{
				extend: "print",
				className: "btn-sm"
			},
			],
			responsive: true
		});
	}
};

Table = function() {    
	return {
		init: function(id) {
			handleDataTableButtons(id);
		}
	};
}();

(function( $ ) {	
	$.fn.CRUD = function(options, t = 'add' ) {

		var self = this;

		var settings = $.extend( {}, {
			url : '',
			type : 'post',
			method : ['post', 'get', 'update', 'delete'],
			finalUrl : options.url,
			validation : true			
		}, options );

		function sendFormData(){
			var formData = new FormData($(self)[0]);

			if(settings.extraVariables){
				$.each(settings.extraVariables(),function(k,v){

					if ($.isArray(v)) {	
						$.each(v,function(d,e) {
							formData.append(k+'[]',e);
						});			
					}else{
						formData.append(k,v);

					}
				});
			}
			settings.type = formData.get('id') ? 2 : (settings.type == '' ? 0 : settings.type);
			switch(settings.type){
				case 0:
				var msgType = (settings.onLoad) ? settings.onLoad : 'add';				
				break;
				case 2:
				settings.finalUrl = settings.url+'/'+formData.get('id');
				formData.append('_method','patch');
				var msgType = (settings.onLoad) ? settings.onLoad : 'update';				
				break;
				case 3:				
				formData.append('_method','DELETE');
				var msgType = (settings.onLoad) ? settings.onLoad : 'deleteS';				
				break;
			}
			// console.log(msgType);
			self.MSG({type:msgType});
			$.ajax({
				url : settings.finalUrl,
				data : formData,
				type : settings.method[0],
				processData : false,
				contentType : false,
				success : function(data){
					(options.processResponse) ? options.processResponse(data) : '';
					self.MSG({type:data.msg});
				},error : function(xhr,data){
					(settings.processError) ? settings.processError(data) : self.MSG({type:data.msg});
				}
			});
		};

		function formSubmission() {			
			$(self).on({
				'submit' : function(e){
					// console.log(self);
					e.preventDefault();
					var check = [false];

					$(self).find('button[type="submit"]').prop('disabled', true);					
					
					$.each($(self)[0],function(k,v){
						var attribute = $(v).attr('data-validate')
						if(attribute){
							var validate = attribute.split('|');
							check.push($.inArray(true,self.VALIDATE({type:validate,name:$(v)})) >= 0);
						}
					});
					if($.inArray(true, check) < 0){						
						// if(!settings.validation){
							if(settings.noAjax){
								$(this)[0].submit();
							}else{
								sendFormData();
							}
						// }
					}
					$(self).find('button[type="submit"]').prop('disabled', false);					
				}
			});  
		}
		
		function fetchData() {
			// console.log(self);
			// var formData = new FormData();

			$.ajax({
				url : settings.url,
				// data : formData,
				type : settings.method[1],
				success : function(data){					
					(options.processResponse) ? options.processResponse(data) : '';
					// self.MSG({type:'success'});
				},error : function(xhr,data){
					(settings.processError) ? settings.processError(data) : self.MSG({type:'error'});
				}
			});
		}

		function edit() {				
			var modal = '#'+options.id+'Modal',
			form = '#'+options.id+'Form';

			var extra = '?';
			if(options.extraVariables){
				$.each(options.extraVariables,function(k,v){
					extra+=k+'='+v;
				});
			}
			
			var editUrl = settings.url+'/'+options.fetchId+'/edit'+extra;	
			
			$(modal).modal('toggle');
			$(modal+'Label').html('Edit '+options.id);
			$(form+' button[type="submit"]').html('UPDATE');

			$.ajax({
				url : editUrl,
				type : settings.method[1],
				success : function(data){
					// console.log(data);
					var input = $('<input/>', {
						value : data.id,
						name : 'id',
						type : 'hidden',
						id : 'id'
					});
					
					$('#id').remove();
					$(form).append(input);
					
					// console.log(data);
					if(data.description){						
						if($('#description').length){
							$("#description .ql-editor").html(data.description);
						}
					}
					$.each($(form)[0].elements, function(index, el) {
						if(el.type == "radio"){
							
							$(el).prop('checked', (el.value == data[el.name]));
						}else if(el.type == "file"){
							var d = data[el.name]
							if(d){
								$('#'+el.name+'Preview').attr('src', mainUrl+d);
							}
						}else if(el.type == "select-multiple"){
							var d = data[el.name.replace('[]','')];
							if(d){
								var selVal = data[el.name.replace('[]','')].split(',');
								$(el).val(selVal).trigger('change');
							}
						}else{
							$(el).val(data[el.name]);						
						}		
						$(el).parent().addClass('is-focused');
					});

					(options.processResponse) ? options.processResponse(data) : '';

				},error : function(xhr,data){
					(settings.processError) ? settings.processError(data) : self.MSG({type:'error'});
				}
			});			
		}

		function deleteForm (){
			$('#'+options.id+'DModal').modal('toggle');
			$('#delName').html(options.name);

			$('#'+options.id+'DForm').CRUD({
				url : options.url+"/"+options.pId,
				type: 3,
				processResponse : function(data){
					console.log(data);
				}
			});
			// console.log('#'+options.id+'DForm');
			// $('#'+options.id+'DForm').CRUD({
			// 	url : options.url+"/"+options.pId,
			// 	type : 3
			// }, 'delete');
		}

		function deleteData() {
			$.ajax({
				url : settings.url,
				data : {'_method' : 'DELETE'},
				type : settings.method[0],
				success : function(data){
					(options.processResponse) ? options.processResponse(data) : '';
				},error : function(xhr,data){
					(settings.processError) ? settings.processError(data) : self.MSG({type:'error'});
				}
			});
		}
		function add (){
			var form = '#'+options.id+'Form';
			$(form)[0].reset();
			$('#'+options.id+'ModalLabel').html('Add '+options.id.replace('-', ''));
			$(form+' button[type="submit"]').html('ADD');			
			$('#'+options.id+'Modal').modal('toggle');			

			
			$(form).CRUD({
				url : options.url				
			});
		}
		this.each(function() {
			switch (t){
				case 'add':
				formSubmission();
				break;
				case 'get':
				fetchData();
				break;
				case 'deleteForm':
				deleteForm();
				break;
				case 'delete':
				deleteData();
				break;
				case 'edit':
				edit();
				break;
				case 'addModal':
				add();
				break;
			}		
		});
		return this;
	};
	
	// $.fn.CRUD.fetchData = function( options ){
	// 	$.fn.CRUD.fetchData(options);
	// };
	
	$.fn.MSG = function( options ){		
		var settings = $.extend( {}, {
			id : '#formAlert',
			msgDetail :function(fetch){				
				var msgs = {
					add : {
						msg : 'Adding, Please wait!!', 
						css : 'warning'
					},
					deleteS : {
						msg : 'Deleting, Please wait!!', 
						css : 'warning'
					},
					check : {
						msg : 'Checking, Please wait!!', 
						css : 'warning'
					},
					update : {
						msg : 'Updating, Please wait!!', 
						css : 'warning'
					},
					success : {
						msg : 'Successfully Added!!', 
						css : 'success'
					},
					successEnquiry : {
						msg : 'We got you Query, we\'ll get back to you soon!!', 
						css : 'success'
					},
					successU : {
						msg : 'Successfully Updated!!', 
						css : 'success'
					},
					successLogin : {
						msg : 'Successfully Logged in!!', 
						css : 'success'
					},
					successRegister : {
						msg : 'Successfully Registered!!', 
						css : 'success'
					},
					successSend : {
						msg : 'Quote Successfully sent!!', 
						css : 'success'
					},
					error : {
						msg : 'Something went wrong, Please try again!!', 
						css : 'danger'
					},
					errorLogin : {
						msg : 'Invalid credentials, Please try again!!', 
						css : 'danger'
					},
					errorD : {
						msg : 'Something went wrong, try again or these data must be linked with other child data!!', 
						css : 'danger'
					},
					exist : {
						msg : 'Record Exist, Please try something new!!', 
						css : 'warning'		
					},
					same : {
						msg : 'Same as Previous, Please try something new!!', 
						css : 'warning'
					},
					delete : {
						msg : 'Record Deleted Successfully!!', 
						css : 'success'		
					}
				}
				return msgs[fetch];
			},
			input : function(fetch){
				var msgs = {
					select : 'Please select option',
					empty : 'These field couldn\'t be Empty',
					mobile : 'Invalid Mobile Number',
					email : 'Invalid Email address',
					alphaSpace : 'Name must contain characters and space',
					minimum : 'Should be a length of 8',
					oneDigit : 'Should contain 1 digit',
					oneLower : 'Should contain 1 Lowercase letter',
					oneUpper : 'Should contain 1 Uppercase letter',
					oneSpecial : 'Must contain 1 Special character like @,$,#,etc...',
					same : 'Password d oesn\'t match',
					digits : 'Please enter digits',
					same : 'Password d oesn\'t match',
					date : 'Invalid Date'
				}
				return msgs[fetch];
			},
			type:'add',
			keep: false,
			milisecond: 1000,
			for : 'form'
		}, options );

		timer = function(process){    		
			window.setTimeout(function(){
				process();
			},settings.milisecond);
		};

		removeInputText = function(name) {    		
			$('#'+name).remove();
		};

		inputText = function(name){
			var span = $('<p/>',{
				id : settings.id,
				style : 'color: #db0c06;',
				html : settings.input(settings.validationType)
			});		
			removeInputText(settings.id);
			$(name).after(span);
		};	

		formText = function(name){
			$.each($(name)[0].elements, function(index, el) {
				$(el).attr('disabled', true);
			});
			$('<div/>',{
				id : 'formAlert',
				class : 'text-center alert alert-'+settings.msgDetail(settings.type).css+' fade in',
				html : settings.msgDetail(settings.type).msg
			}).appendTo(name);
			var successText = ['success', 'successU', 'delete','successEnquiry'];
			var errorText = ['error', 'errorU'];
			// if (!settings.keep) {
				var formId = $(settings.id).parent().attr('id').replace('Form','');
				timer(function(){
					$.each($(name)[0].elements, function(index, el) {
						$(el).attr('disabled', false);
					});	
					if ($.inArray(settings.type, successText) > -1) {						
						$(name)[0].reset();
						if($('#'+formId+'Modal')){
							$('#'+formId+'Modal').modal('toggle');;
						}
						$(settings.id).remove();
					}
				});
				$(name).find('button[type="submit"]').prop('disabled', true);
			// }
		};

		return this.each(function(){  

			if($(settings.id).length){
				$(settings.id).remove();
			}

			switch(settings.for){
				case 'form' :
				formText(this);
				break;

				case 'input' :
				inputText(this);
				break;				
				
				case 'remove' :
				removeInputText(settings.id);
				break;
			}			
		});
	};

	$.fn.VALIDATE = function (options) {
		var settings = $.extend( {}, {    		
			alphaSpace : /^[a-zA-Z\s]+$/,
			empty : /^$/,
			email : /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/,
			mobile : /^\d{10}$/,
			digits : /^\d$/,
			select : /^-1$/,
			date : /^([0-9][1-2])\/([0-2][0-9]|[3][0-1])\/((19|20)[0-9]{2})$/
		}, options );

		function processValidate(name,v) {
			if(!name.attr('disabled')){
				var pattern = (( v == 'empty') ? (settings[v].test($(name).val())) : (!settings[v].test($(name).val())));
				
				if( v=='select' ){
					pattern = (settings[v].test($(name).val()));
				}
				
				if( v == 'digits' ){
					pattern = (settings[v].test($(name).val()));
				}
				if (pattern){
					$(name).MSG({id:$(name).attr('name')+v,validationType:v,for : 'input'}).focus();
					return true;
				}
				$(name).MSG({id:$(name).attr('name')+v,validationType:v,for : 'remove'});
				return false;			
			}
		}

		var chk = [];

		$.each(settings.type,function(k,v){    		
			chk.push(processValidate(settings.name,v));    			
		});

		return chk;    	
	}

}( jQuery ));

var height,width;

function imageUpload(id) {

	$('#'+id).on("change", function(){
		var files = !!this.files ? this.files : [];

		if (!files.length || !window.FileReader) return;

		if (/^image/.test( files[0].type)){
			var reader = new FileReader();
			reader.readAsDataURL(files[0]);
			var sizeMB = files[0].size / (1024 * 1024);
			if(sizeMB > 2){
				$('#'+id).parent().parent().parent().find('button[type="submit"]').prop('disabled', true);
				$('#'+id).parent().append('<p style = "color: red;margin-top: 10px;">Please select lessthan 2 MB Image.</p>');
			}else{
				$('#'+id).parent().find('p').remove();
				$('#'+id).parent().parent().parent().find('button[type="submit"]').prop('disabled', false);				
			}
			reader.onloadend = function(upImg){

				$("#"+id+"Preview").attr("src", this.result);
				urlValue = this.result;
				
				var image = new Image();
				image.src = upImg.target.result;

				image.onload = function() {
					height = this.height;
					width = this.width;					
				};
			}                
		}
	});
}
// if(data.length){
// var tr = [];
// var th = [];
// var ke = Object.keys(data[0]);
//
// var seq = ['title','hours',];
// $.each(data,function(k,v){
// 	var keys = Object.keys(v);
// 	var objectLength = keys.length;
// 	var td = [];

// 	for(var i = 0; i < objectLength; i++){
// 		td.push($('<td/>', {
// 			html : v[keys[i]]
// 		}));
// 	}
// 	tr.push($('<tr/>', {
// 		html : td
// 	}));
// });

// }

function multiSelect(id, link, type = null) {
	var self = id;
	$(id).select2({
		tags:true,
		placeholder: 'Start typing slowly',
		allowClear: true,
		createTag: function (params) {
			var term = $.trim(params.term);

			if (term === '') {
				return null;
			}

			return {
				id: term,
				text: term,
				newTag: true
			}
		}
	});
	$(id).on('select2:select', function (e) {
		var data = e.params.data;
		var old = data.id;
		if(data.newTag){
			$.post(geturl(link), {title: data.text, type : type}, function(data) {
				$(self).find("option[value='" + old + "']").remove();
				var newOption = new Option(data.d.title, data.d.id, true, true);
				$(self).append(newOption).trigger('change');				
			});
		}
	});
}

function closeModal(id){
	$('#'+id+'Modal').modal('toggle');
}


function selectBox(first, second, url) {
	$('#'+first).on({
		change : function(){
			var value = this.value;
			if(value != '-1'){
				$('#'+second).removeClass('hidden');
				$.ajax({
					url: url,
					type : 'GET',
					data: {id: value},
				}).done(function(data) {
					if(data.length){					
						var d = ['<option value = "-1">--select--</option>'];
						if(data) {
							$.each(data, function(k,v){
								var li = $('<option/>',{
									html : v.title,
									value : v.id
								});
								d.push(li);
							});
							$('#'+second).removeClass('hidden').addClass('is-focused');							
							$('#'+second+' select').html(d).prop('disabled', false);
							$('.'+second+'Drop').select2();
						}
					}else{
						$('#'+second).addClass('hidden');
					}
				});				
			}else{
				$('#'+second).addClass('hidden');				
			}			
		}
	});
}

function getSelectValues(main, sub, ph){	
	$('#'+main).on({
		'change' : function(){
			var val = this.value;
			if(val == '-1'){
				$('#'+sub).prop('disabled',true);
			}else{

				var p = ph+'/'+val;
				$.ajax({
					url : p,
					method : 'get',
					dataType : 'json',
					success : function (data) {
						var options = [];
						options.push('<option value = "-1">--Select--</option>');
						if(data.length > 0){
							$.each(data,function(k,v){
								options.push('<option value = "'+v.id+'">'+v.name+'</option>');
							});
							$('#'+sub).html(options).prop('disabled',false);
						}else{
							$('#'+sub).prop('disabled',true);
						}
					}			
				});
			}
		}
	});
}